---
title: "STAT 641 - HW 6"
author: "Ricardo Batista"
date: "10/23/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Question 2.28

## (i)

&nbsp;&nbsp;&nbsp;&nbsp; Recall that $f_n \rightarrow f$ a.e. ($\mu$) if there exists $B \in F$ such that $\mu(B) = 0$ and 

$$
\begin{aligned}
\lim_{n \rightarrow \infty} f_n(x) = f(x) && \text{for all } x \in B^c
\end{aligned}
$$

We show that $\lim_{n \rightarrow \infty} f_(x) = f(x)$. Let $\varepsilon > 0$ be given. We seek to show that there exists some $N_\varepsilon$ such that 

$$
n \geq N_\varepsilon \hspace{15pt} \implies \hspace{15pt} \vert f_n(x) - f(x) \vert < \varepsilon.
$$

&nbsp;&nbsp;&nbsp;&nbsp; Note 

$$
\vert f_n(x) - f(x) \vert 
= \vert I_{(0, 1/n)}(x) - n I_{(-1/n, 0)}  - 0 \vert = \vert n I_{(-1/n, 1/n)} (x) \vert
$$

so setting $N_\varepsilon$ such that $\frac{1}{N_\varepsilon} < \vert x \vert$ yields

$$
\vert N_\varepsilon I_{(-1/N_\varepsilon, 1 / N_\varepsilon)}(x) \vert = 0.
$$

since 

$$
n \geq N_\varepsilon \hspace{15pt} \implies \hspace{15pt} \frac{1}{n} < \frac{1}{N_\varepsilon} < x,
$$

$f_n(x) = 0$ for all such $n$. (Note the set $B$ in question is $B = \{\emptyset\}$.)

\pagebreak

## (ii)

&nbsp;&nbsp;&nbsp;&nbsp; We seek to show that $\int f_n d\mu \rightarrow \int f d\mu$. Let $\varepsilon > 0$ be given. We week to show $\exists N_\varepsilon$ such that

$$
n \geq N_\varepsilon \hspace{15pt} \implies \hspace{15pt} 
\left\vert \int f_n d\mu - \int f d\mu \right\vert < \varepsilon.
$$

Recall $f = 0$, so $\int f d\mu = 0$. Now note

$$
\begin{aligned}
\left\vert \int f_n d\mu \right\vert
&= \left\vert \int f_n^+ d\mu - \int f_n^- d\mu \right\vert
=  \left\vert n \mu\left(\left(0, \frac{1}{n}\right)\right) 
              - \left[ - \left(- n \mu\left(\left(-\frac{1}{n}, 0\right)\right)\right)\right]
\right\vert\\[10pt]
&= \left\vert n \left(\frac{1}{n}\right) - n \left(\frac{1}{n}\right)\right\vert = 0.
\end{aligned}
$$

Since $\int f_n = 0$ for all $n$, we have that setting $N_\varepsilon$ to be any $n \in \mathbb{N}$, 

$$
n \geq N_\varepsilon \hspace{15pt} \implies \hspace{15pt} 
\left\vert \int f_n d\mu - \int f d\mu \right\vert = 0 < \varepsilon.
$$

\pagebreak

## (iii)

&nbsp;&nbsp;&nbsp;&nbsp; Recall that the sequence of $\langle \mathcal{F}, \mathcal{B}(\mathbb{R})\rangle$ functions $\{f_\lambda : \lambda \in \Lambda\}$ on measure space $(\Omega, \mathcal{F}, \mu)$ is uniformly integrable (i.e., UI) if 

$$
\sup_{\lambda \in \Lambda} a_\lambda (t) \rightarrow 0 
\hspace{15pt} \text{as } t \rightarrow \infty
$$

where

$$
a_\lambda(t) = \int_{\{\vert f_\lambda \vert > t\}} \vert f_\lambda \vert d\mu
$$

and $\{\vert f_\lambda \vert > t\} \equiv \{\omega : \vert f_\lambda (\omega) \vert > t\}$.  

&nbsp;&nbsp;&nbsp;&nbsp; For the case in question,

$$
\begin{aligned}
a_n(t) 
&= \int_{\vert f_n(x) \vert > t} \vert f_n \vert dm
=  \int_{\vert f_n(x) \vert > t} \left\vert n I_{(0, 1/n)} - n I_{(-1/n, 0)} \right\vert dm\\[10pt]
&= \int_{\vert f_n(x) \vert > t} n I_{(-1/n, 1/n)}
=  n \mu \left(\{ x: n I_{(-1/n, 1/n)}(x) > t\}\right)\\[10pt]
&= n \left(\frac{1}{n} - \left(\frac{1}{n}\right)\right)I_{\{t < n \}} (t) = 2 I_{\{t < n \}} (t).
\end{aligned}
$$

So $a_n(t) = 2 I_{\{t < n \}}(t)$. Then

$$
\sup_{n \geq 1} a_n(t) = \sup_{n \geq 1} 2 I_{\{t < n \}}(t)
$$

and any $n > t$ will produce the supremum. Let $\lceil t \rceil$ be such an integer. Then

$$
\sup_{n \geq 1} a_n(t) = a_{\lceil t \rceil} (t) = 2.
$$

Since for any real number, there exists an integer greater than it,

$$
\sup_{n \geq 1} a_n (t) = 2
\hspace{15pt} \text{as } t \rightarrow \infty.
$$

\pagebreak

# Question 2.30

## (i)

&nbsp;&nbsp;&nbsp;&nbsp; Recall that $f_n \rightarrow f$ a.e. ($m$) means $\exists B \in \mathcal{B}(\mathbb{R})$ such that $\mu(B) = 0$ and 

$$
\lim_{n \rightarrow \infty} f_n(\omega) = f(\omega)
\hspace{15pt}
\text{for all } x \in B^c.
$$

Let $B = (-\infty, 0]$. We now consider $x \in (0, \infty)$. Let $\varepsilon > 0$; we seek $N_\varepsilon$ such that 

$$
n \geq N_\varepsilon \hspace{15pt} \implies \hspace{15pt}
\vert f_n (\omega) - f(\omega) \vert < \varepsilon.
$$

Note $f(\omega) = 0$, so

$$
\vert f_n(x) \vert = \left\vert \frac{1}{\sqrt{n}} I_{(0, n)} (x)\right\vert
= \frac{1}{\sqrt{n}} I_{(0, n)} (x)
$$

Note $1/\sqrt{n} \rightarrow 0$, so we select $N_\varepsilon$ such that $1 / \sqrt{N_\varepsilon} < \varepsilon$. Note that for $n \geq N_\varepsilon$, 

$$
\frac{1}{\sqrt{n}} I_{(0, n)} (x) 
\leq \frac{1}{\sqrt{N_\varepsilon}} I_{(0, N_\varepsilon)} (x) < \varepsilon.
$$

## (ii)

&nbsp;&nbsp;&nbsp;&nbsp; Recall that for each $n \geq 1$ we have

$$
\begin{aligned}
a_n(t) &= \int_{\{\vert f_n \vert > t\}} \vert f_n \vert dm 
= \int_{\{\vert f_n \vert > t\}} \left\vert \frac{1}{\sqrt{n}} I_{(0, n)} \right\vert dm 
= \frac{1}{\sqrt{n}} \int_{\{\frac{1}{\sqrt{n}} I_{(0, n)} > t\}} I_{(0,n)}\\[10pt]
&= \frac{1}{\sqrt{n}} \mu((0, n)) I_{\{t < \frac{1}{\sqrt{n}}\}} (t)
= \sqrt{n} I_{\{t < \frac{1}{\sqrt{n}}\}} (t).
\end{aligned}
$$

Then, since $\sup_{n \geq 1} \tfrac{1}{\sqrt{n}} = 1$, we have that $a_n(1) = 0$ for all $n \geq 1$. Therefore, 

$$
\sup_{n \geq 1} a_n (t) = 0 \hspace{15pt} \text{as } t \rightarrow \infty.
$$

## (iii)

&nbsp;&nbsp;&nbsp;&nbsp; Note that

$$
\int f_n d\mu = \frac{1}{\sqrt{n}} \mu((0, n)) = \sqrt{n} 
\leq \int f_{n + 1} d\mu = \sqrt{n + 1}.
$$

So $\{\int f_n\}_{n \geq 1}$ diverges, and, since $\int f d\mu = 0$, 

$$
\int f_n d\mu \nrightarrow \int f d\mu.
$$

\pagebreak

# Question 2.36

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; Let $\varepsilon > 0$ be given. Note that

$$
f_n \rightarrow 0 \hspace{15pt} \implies \hspace{15pt} 
\lim_{n \rightarrow \infty} \int_{\Omega} \vert f_n \vert d\mu = 0.
$$

Therefore, there exists $N_\varepsilon$ such that 

$$
\sup_{n \geq N_\varepsilon} \int_\Omega \vert f_n \vert d\mu < \varepsilon.
$$

Now we then have that, by DCT, there exists $t_\varepsilon > 0$ such that

$$
\sup_{1 \leq n \leq N_\varepsilon} \int_{\{\vert f_n \vert > t\}} \vert f_n \vert d\mu 
< \varepsilon
$$

for all $t > t_\varepsilon$. In other words, for every $\varepsilon > 0$, there exists $t_\varepsilon > 0$ such that for all $t > t_\varepsilon$, 

$$
\sup_{n \geq 1} \int_{\{\vert f_n \vert > t\}} \vert f_n \vert d\mu
\leq \max \left( 
\sup_{1 \leq n \leq N_\varepsilon} \int_{\{\vert f_n \vert > t\}} \vert f_n \vert d\mu, 
\sup_{n \geq N_\varepsilon} \int_{\Omega} \vert f_n \vert d\mu \right)
\leq \varepsilon.
$$

That is, $\{f_n\}_{n \geq 1}$ is UI.  

\pagebreak


## (b)

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{f \in  L^p(\mu)}$

&nbsp;&nbsp;&nbsp;&nbsp; By Theorem 2.5.2 we have that since $f_n \rightarrow ^m f$, then there exists a subsequence $\{n_k\}_{k \geq 1}$ such that $f_{n_k} \rightarrow f$ a.e. ($\mu$). Then, by the power rule of limits, $\vert f_{n_k} \vert^p \rightarrow \vert f \vert$ a.e. ($\mu$). Now, by Theorem 2.5.10, since $\mu(\Omega) < \infty$, $\{\vert f_{n_k} \vert^p\} \subset L^1$ such that $\vert f_{n_k}\vert^p \rightarrow \vert f \vert^p$ a.e. ($\mu$), $\vert f \vert^p$ is measurable, and $\{\vert f_{n_k} \vert^p\}$ is UI, $\vert f \vert^p$ is integrable, i.e., 

$$
\int \vert f \vert^p d\mu < \infty.
$$

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{f_n \rightarrow f \text{ in }  L^p(\mu)}$

&nbsp;&nbsp;&nbsp;&nbsp; Recall 

$$
a_n(t) \equiv \int_{\{\vert f_n \vert >t \}} \vert f_n \vert d\mu.
$$

Now consider

$$
\begin{aligned}
\int_{\left\{\vert f_n + f \vert^p > t \right\}} 
      \vert f_n + f \vert^p d\mu
& \leq \int_{\left\{\left(2 \cdot \max (\vert f_n \vert, \vert f \vert) \right)^p > t \right\}} 
      \left(2 \cdot \max (\vert f_n \vert, \vert f \vert) \right)^p d\mu\\[10pt]
& \leq \int_{\{2^p \vert f_n \vert^p > t \}} 
            2^p \vert f_n \vert^p d\mu + 
       \int_{\{2^p \vert f \vert^p > t \}} 
            2^p \vert f \vert^p d\mu\\[10pt]
&\leq
\sup_{n \geq 1} 
    \int_{\left\{\vert f_n \vert^p > \frac{t}{2^p} \right\}} 
        \vert f_n \vert^p d\mu +
\sup_{n \geq 1} 
    \int_{\left\{\vert f \vert^p > \frac{t}{2^p} \right\}} 
        \vert f_n \vert^p d\mu
\end{aligned}
$$

which reveals that $\{\vert f_n + f \vert^p : n \geq 1\}$ is UI.

&nbsp;&nbsp;&nbsp;&nbsp; Let $\varepsilon, t > 0$, then 

$$
\begin{aligned}
\int \vert f_n - f \vert^p d\mu &=
\int_{\{\vert f_n - f \vert ^p \leq \varepsilon \}} \vert f_n - f \vert^p d\mu +
\int_{\{\varepsilon < \vert f_n - f \vert ^p \leq t \}} \vert f_n - f \vert^p d\mu +
\int_{\{\vert f_n - f \vert ^p > t\}} \vert f_n - f \vert^p d\mu\\[10pt]
&\leq \varepsilon^p \mu(\Omega) + 
t^p \mu( \{\omega : \vert f_n (\omega) - f(\omega) \vert > \varepsilon\}) +
\sup_{n \geq 1} \int_{\{\vert f_n - f \vert ^p > t\}} \vert f_n - f \vert^p d\mu
\end{aligned}
$$

taking $\lim \sup$,

$$
\begin{aligned}
\lim_{n \rightarrow \infty} \sup \int \vert f_n - f \vert^p d\mu 
&\leq
\lim_{n \rightarrow \infty} \sup \varepsilon^p \mu(\Omega) + 
t^p \mu( \{\omega : \vert f_n (\omega) - f(\omega) \vert > \varepsilon\}) +
\sup_{n \geq 1} \int_{\{\vert f_n - f \vert ^p > t\}} \vert f_n - f \vert^p d\mu\\[10pt]
\lim_{n \rightarrow \infty} \sup \int \vert f_n - f \vert^p d\mu 
&\leq
 \varepsilon^p \mu(\Omega) + 
\sup_{n \geq 1} \int_{\{\vert f_n - f \vert ^p > t\}} \vert f_n - f \vert^p d\mu.
\end{aligned}
$$

Now, let $\varepsilon \rightarrow 0$ and $t \rightarrow \infty$. Since $\mu(\Omega) < \infty$ and $\{\vert f_n - f \vert^p : n \geq 1\}$ is UI,

$$
\begin{aligned}
\lim_{n \rightarrow \infty} \sup \int \vert f_n - f \vert^p d\mu = 0.
\end{aligned}
$$

\pagebreak

# Question 2.42

&nbsp;&nbsp;&nbsp;&nbsp; First we consider $h = I_A$, i.e., $h$ is an indicator function. Recall that $I(x) \in L^1(\mathbb{R}, \mathcal{B} \mathbb{R}, \mu_3)$ means

$$
\int_{\mathbb{R}} I(x) \mu_3(dx) = \int_{\mathbb{R}} x d\mu_3 <\infty
$$

Now this is true iff

$$
\mu_3(A) = \mu_2 \circ (I_A (A))^{-1} <\infty
$$

which means

$$
\int_{\Omega_2} I_{(I_A (A))^{-1}}(\omega_2) \mu_2(d\omega_2) < \infty.
$$

This is equivalent to saying $I_A \in L^1(\mu_2)$, which means

$$
\int_{\Omega_2} I_A(\omega_2)\mu_2(d\omega_2) < \infty.
$$

Now, this is true iff

$$
\mu_2(A) = \mu_1 \circ f^{-1}(A) < \infty
$$

which means

$$
\int_{\Omega_1} I_{f^{-1}(A)}(\omega_1) \mu_1(d\omega_1) < \infty.
$$

Finally, we have that the above is true iff $I_A \circ f \in L^1(\Omega_1)$

So then we have that

$$
\begin{aligned}
&\int_{\Omega_1} I_A \circ f(\omega_1) \mu_1 (d \omega_1) = 
\int_{\Omega_1} I_{f^{-1}(A)}(\omega_1) \mu_1 (d \omega_1) =
\mu_1(f^{-1}(A)) = \mu_1 \circ f^{-1}(A) = \mu(A)\\[10pt]
&= \int_{\Omega_2} I_A(\omega_2) \mu_2(d\omega_2)
= \int_{\Omega_2} I_{(I_A (A))^{-1}}(\omega_2) \mu_2(d\omega_2)
= \mu_2 \circ (I_A (A))^{-1} = \mu_3(A)\\[10pt]
&=\int_{\mathbb{R}} I(x) \mu_3(dx),
\end{aligned}
$$

which proves what we set out to (for indicator functions and, by extension, simple nonnegative functions).  

&nbsp;&nbsp;&nbsp;&nbsp; Now we consider $h$ to be a nonnegative measurable function. Let $\{h_n\}_{n \geq 1}$ be an increasing sequence of nonnegative mesaurable functions such that $h_n \rightarrow h$. Then, by MCT and our work above,

$$
\begin{aligned}
&\int Id\mu_3 
= \lim_{n \rightarrow \infty} \int_{\mathbb{R}} I_n d\mu_3
= \lim_{n \rightarrow \infty} \int_{\Omega_2} I_n \circ h d \mu_2
= \int_{\Omega_2} I \circ h d \mu_2\\[10pt]
&=\int_{\Omega_2} h d\mu_2
= \lim_{n \rightarrow \infty} \int_{\Omega_2} h_n d\mu_2
= \lim_{n \rightarrow \infty} \int_{\Omega_1} h_n \circ f d\mu_1
= \int_{\Omega_1} h \circ f d \mu_1\\[10pt]
&= \int_{\Omega_1} g d \mu_1.
\end{aligned}
$$

which proves what we set out to (for nonnegative measurale functions).

&nbsp;&nbsp;&nbsp;&nbsp; Now, for $h \in L^1(\Omega_2)$,

$$
\begin{aligned}
&\int_\mathbb{R}I d \mu_3
= \int_{\Omega_2} I \circ h d\mu_2\\[10pt]
& =\int_{\Omega_2} h d\mu_2
= \int_{\Omega_2} h^+ d\mu_2 - \int_{\Omega_2} h^- d\mu_2
= \int_{\Omega_1} h^+ \circ f d\mu_1 - \int_{\Omega_1} h^- \circ f d\mu_1
= \int_{\Omega_1} (h^+ - h^-) \circ f d \mu_1\\[10pt]
&=  \int_{\Omega_1} g d \mu_1.
\end{aligned}
$$

